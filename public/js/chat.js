// Websocket Code
$(function () {
  let socket = io();
  let get_cookie_field = (field) => {
    let cookie_fields = document.cookie.split(";");
    let field_value = undefined;
    for (i=0; i<cookie_fields.length; i++ ) {
      let cookie_field = cookie_fields[i].split("=");
      if (cookie_field[0].trim() == field) field_value=cookie_field[1];
    }
    return field_value;
  }

  let username = get_cookie_field("chat_username");
  let user_id = get_cookie_field("chat_user_id");

  // Replace certain text combinations with unicode equivalent
  let emoji_shortcut_unicode_replace = (msg) => {
    msg = msg.replace(":)", "&#x1F601");  // beaming face with smiling eyes
    msg = msg.replace(":D", "&#x1F601");  // beaming face with smiling eyes
    msg = msg.replace(":(", "&#x1F641");  // slightly frowning face
    msg = msg.replace(":o", "&#x1F632");  // astonished face
    msg = msg.replace("xD", "&#x1F606");  // grinning squiting face
    msg = msg.replace(":P", "&#x1F61B");  // face with tongue
    msg = msg.replace(":Z", "&#x1F910");  // zipper-mouth face
    msg = msg.replace("._.", "&#x1F610"); // neutral face
    msg = msg.replace("-_-", "&#x1F611"); // expressionless face
    msg = msg.replace("8)", "&#x1F60E");  // smiling face with sunglasses
    msg = msg.replace("8^)", "&#x1F913"); // nerd face
    msg = msg.replace(":/", "&#x1F615");  // confused face
    msg = msg.replace(":`(", "&#x1F622"); // crying face
    msg = msg.replace(":S", "&#x1F616");  // confounded face
    msg = msg.replace("<3", "&#x2764");   // red heart
    msg = msg.replace("</3", "&#x1F494"); // broken heart
    return msg;
  }

  // Twemoji emoji rendering
  let twemoji_render = (element) => {
      twemoji.parse(element, {
      folder: 'svg',
      ext: '.svg',
      callback: function(iconId, options) {
          switch (iconId) { // ignores the copyright, registered trademark, and trademark symbols
                case 'a9':      // © copyright
                case 'ae':      // ® registered trademark
                case '2122':    // ™ trademark
                  return false;
          }
          return ''.concat(options.base, options.size, '/', iconId, options.ext); // actually renders the emoji
      }
    });
  }

  // JQuery UI success/fail updates
  let chat_success = () => {
    let success_color = '#00C000';
    $("#chat-input").effect( "highlight", options={color:success_color} );
  }
  let chat_error = () => {
    let error_color = '#F00000';
    $("#chat-input").effect( "highlight", options={color: error_color} ).effect( "shake" ); // Error Shake
  }

  // Parse user commands
  let user_command = function(){
    let tokenized_input = $('#chat-input').val().split(" ");
    let user_cmd = tokenized_input.shift();
    switch(user_cmd){
      case("/name"):
        let new_username = tokenized_input.join(' ');
        socket.emit('username change request', new_username, function (success, msg) {
          if(success) {
            username = new_username;
            document.cookie = "chat_username=" + new_username;
            chat_success();
            console.log("Successfully updated username")
          } else {
            chat_error();
            console.log("Couldn't change username:", msg);
          }
        });
        break;

      case("/color"):
        if(tokenized_input.length != 1) {
          chat_error();
          console.log("Expecting only 1 argument for color: 'RRGGBB'")
          break;
        }
        socket.emit('user color change request', tokenized_input[0], function (success, msg) {
          if(success) {
            chat_success();
            console.log("Successfully updated user color")
          } else {
            chat_error();
            console.log("Couldn't update user color:", msg);
          }
        });
        break;

      default:
        console.log("Unknown command: " + user_cmd);
        chat_error();
    }

  };

  // User Input Action
  $('form').submit(function(e) {
    e.preventDefault(); // prevents page reloading
    // Check for command escape char
    if ($('#chat-input').val()[0] === '/'){
      user_command();
    }
    // Otherwise, if input isn't empty send chat message
    else if($('#chat-input').val() != ''){
      socket.emit('chat message', $('#chat-input').val());
    }
    $('#chat-input').val('');
  });

  // Update chat window
  socket.on('chat update', (chat_history) => {
    $('#messages-window').html('');
    for(let i=0; i<chat_history.length; i++){
      let timestamp = chat_history[i][0];
      let msg = emoji_shortcut_unicode_replace(chat_history[i][1]);
      // let msg = chat_history[i][1];
      let user = chat_history[i][2];
      let msg_color = chat_history[i][3];
      let new_message_elem = document.createElement('div');
      let message_content = "<hr/><span style='color:" + msg_color + "'>"
      message_content += user + " says: <small>[" + timestamp + "]</small><br/>"+ msg
      message_content += "</span><br/>";
      if(user === username){
        message_content = "<strong>" + message_content + "</strong>";
      }
      $(new_message_elem).html(message_content);
      twemoji_render(new_message_elem);
      $('#messages-window').prepend($(new_message_elem));
      new_message_elem.scrollIntoView();
    }
    // twemoji_render(document.getElementById('messages-window'))
  });

  // Update to userlist
  socket.on('userlist update', (userlist) => {
    $('#user-list').html('');
    for(const user in userlist){
      let new_user_elem = document.createElement('li');
      let _user_html = '<span style="color:' + userlist[user] + '">' + user + ' </span>';
      if (user === username) {
        _user_html = '<span class="current-user">' + _user_html + ' </span>'
        $('#current-username').html(_user_html);
      }
      $(new_user_elem).html(_user_html);
      $('#user-list').append($(new_user_elem));
      $('#user-list').append("<hr>");
    }
  });

  // Server status change, reload
  socket.on('server update', () => {
    location.reload();
  });
});

// UI Code
(function(){
  // Sidenav Drawer functions for userlist
  function openNav() {
    $("#userlist-drawer").toggleClass('active');
    $("#userlist-drawer-modal").toggleClass('active');
  }
  function closeNav() {
    $("#userlist-drawer").toggleClass('active');
    $("#userlist-drawer-modal").toggleClass('active');
  }

  // Register Callbacks
  $("#userlist-btn").click(openNav);
  $('#userlist-close-btn').click(closeNav);
  $("#userlist-drawer-modal").click(closeNav);

  function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
  }

  // Randomized background image
  // Photo Credits:
  // All photos retrieved under commercial use licence from unsplash.com
  // Background 1 - Photo by Wassim Chouak
  // Background 2 - Photo by Etienne Godiard
  // Background 3 - Photo by Josh Applegate
  // Background 4 - Photo by Jansen Yang
  // Background 5 - Photo by Hena Sheik
  // Background 6 - Photo by Ksenia Makagonova
  // Background 7 - Photo by Daniel Halseth
  // Background 8 - Photo by Hidayat Abisena
  // Background 9 - Photo by Tim J
  // Background 10 - Photo by Mike Petrucci
  let url = "url('/media/background-" + getRndInteger(1, 10) + ".jpg')"
  document.body.style.backgroundImage = url;
})();
