(function(){
  // Server Requirements
  const cookieParser = require('cookie-parser');
  let express = require('express');
  let app = express();
  let server = require('http').createServer(app);
  let io = require('socket.io')(server);
  const { uniqueNamesGenerator, adjectives, colors, animals } = require('unique-names-generator');

  // Username and ID Assignment
  let user_list = {
    known_users     : {},   // {User ID: Username} All known users
    user_colors     : {},   // {User ID: '#RRGGBB'} Custom user colours
    connected_users : {},   // {User ID: conn_instances} All currently connected users

    // Register a user as connected and emit an updated connected userlist
    connect   : function(username, user_id) {
      console.log(username + '[id: ' + user_id + '] connected.');

      this.connected_users[user_id] = user_id in this.connected_users ? this.connected_users[user_id] + 1 : 1;
      this.known_users[user_id] = username;

      console.log("Current users: " + JSON.stringify(this.get_connected_users()));
      console.log("Known users: " + this.get_known_users());
      io.emit("userlist update", this.get_connected_users());
    },

    // Unregister a user from connected and emit an updated connected userlist
    disconnect: function(username, user_id) {
      console.log(username + '[id: ' + user_id + '] disconnected');
      if (! user_id in this.connected_users){
        console.log("ERROR: Trying to delete user '" + user_id + "' from user list, but doesn't exist.")
        return ;
      }
      this.connected_users[user_id] -= 1;
      if (this.connected_users[user_id] < 1) delete this.connected_users[user_id];
      console.log("Current users: " + JSON.stringify(this.get_connected_users()));
      io.emit("userlist update", this.get_connected_users());
    },

    // Get a list of currently connected users by {username: color preference}
    get_connected_users : function() {
      let conn_users_ids = Object.keys(this.connected_users);
      let conn_usernames = {};
      for(let i = 0; i < conn_users_ids.length; i++){
        let __id = conn_users_ids[i];
        let __name = this.known_users[__id]
        let __color = this.get_user_color(__id);
        conn_usernames[this.known_users[conn_users_ids[i]]] = __color
      }
      return conn_usernames;
    },

    // Get a list of currently known users by {id: username}
    get_known_users     : function() {
      return JSON.stringify(this.known_users);
    },

    // Add a user to this.known_users
    add_known_user      : function(username, user_id){
      if(this.requires_reset(username, user_id)){
        console.log("ERROR: Trying to add invalid user " + username + " (ID: " + user_id + ")")
        return false;
      }
      this.known_users[user_id] = username;
    },

    // Generate a unique random username
    generate_new_name   : function() {
      let new_username = undefined;
      while(true){
        new_username = uniqueNamesGenerator({
          dictionaries: [adjectives, colors, animals],
          length: 3
        });
        if(this.is_valid_name(new_username)) break;
      }
      return new_username;
    },

    // Check if a given username/user id pair requires a reset
    requires_reset      : function(username, user_id) {
      // Check to ensure that the user_id/username pair is still valid
      if(this.known_users[user_id] !== undefined && this.known_users[user_id] !== username){
        console.log("User ID exists but username doesn't match: " + this.known_users[user_id] )
        return true;
      }
      // Do one last an extra check of all usernames to ensure no one took the username
      for(_uid in this.known_users){
        if(this.known_users[_uid] === username && _uid != user_id){
          console.log("Found the username but UID doesn't match:", _uid, username, user_id, "(reset required)")
          return true;
        }
      }
      return false;
    },

    // Check if a given username is valid (i.e. unique)
    is_valid_name       : function(username) {
      return !(Object.values(this.known_users).includes(username));
    },

    // Get the specified user's hex color code. Default #000000 (black)
    get_user_color      : function(user_id){
      let user_color = this.user_colors[user_id];
      return user_color !== undefined ? user_color : "#000000";  // Default black
    },

    // Get user ID from username
    get_user_id         : function(username){
      for(uid in this.known_users){
        if(this.known_users[uid] === username) return uid;
      }
      console.log("ERROR: Couldn't find user ID for username: " + username)
      return undefined;
    },

    // Get username from user id
    get_name_from_id    : function(user_id){
      return this.known_users[user_id];
    },

    // Generate an unassigned user id
    get_unassigned_id   : function(){
      // Create a unique user ID based on the epoch time of the request
      // and a random number.
      let new_id = undefined;
      while(true){
        new_id = "" + Date.now() + "-" + Math.floor(Math.random() * 1000);
        if(! (new_id in this.known_users) ) break;
      }
      console.log("---NEW ID", new_id)
      return new_id;
    },

    // Update username for the specified user id
    update_username     : function(user_id, new_username){
      if(! user_id in this.known_users){
        console.log("ERROR: Trying to update username for unknown user ID");
        return {success:false, message:"Trying to update username for unknown user ID"};
      }
      this.known_users[user_id] = new_username;
      console.log("Username changed", JSON.stringify(this.known_users));
      return {success:true, message:"Username updated successfully."};
    },

    // Update color for the specified user id
    update_color        : function(user_id, new_color){
      if(! user_id in this.known_users){
        console.log("ERROR: Trying to update username for unknown user ID");
        return {success:false, message:"Invalid user ID"};
      }
      let color_regex = new RegExp("^[a-fA-F0-9]{6}$");
      let valid_color = color_regex.test(new_color);  // TODO move valid check to update methos
      if(!valid_color) {
        return {success:false, message:"Invalid color code supplied. Expecting RRGGBB"};
      }
      this.user_colors[user_id] = "#" + new_color;
      return {success:true, message:"Successfully updated color"};
    }
  };

  // Chat log
  let chat_log = {
    _max_log_length: 200, // How many messages to keep in history
    message_history: [],  // Newest msg at last index, oldest at last first

    // Add a new message to the chat log and emit the chat history
    new_message: function(timestamp, msg, username){
      let user_id = user_list.get_user_id(username);
      this.message_history.push([timestamp, msg, user_id]);
      while(this.message_history.length > this._max_log_length) {
        this.message_history.shift();
      }
      this.send_history();
    },

    // Send chat history to the specified socket, or broadcast to all sockets
    // if socket is undefined
    send_history: function(socket){
      // Package up the message history to include username and color information
      let message_broadcast = []
      for(let i = 0; i < this.message_history.length; i++){
        this.message_history[i]
        let _msg_time = this.message_history[i][0];
        let _msg_content = this.message_history[i][1];
        let _msg_name = user_list.get_name_from_id(this.message_history[i][2]);
        let _msg_color = user_list.get_user_color(this.message_history[i][2]);
        message_broadcast.push([_msg_time, _msg_content, _msg_name, _msg_color]);
      }

      if(socket !== undefined){  // Only send to a single socket
        socket.emit('chat update', message_broadcast);
      } else {  // Broadcast to everyone
        io.emit('chat update', message_broadcast);
      }
    }
  }

  // Parse the provided http cookie string for the specified field's value
  const parse_cookie_value = function(http_cookie, field="chat_username") {
    let cookie_fields = http_cookie.split(";");
    let username = undefined;
    for (i=0; i<cookie_fields.length; i++ ) {
      let cookie_field = cookie_fields[i].split("=");
      if (cookie_field[0].trim() == field) username=cookie_field[1];
    }
    return username;
  };

  // Middleware: Cookie Parser
  app.use(cookieParser());

  // Middleware: Check cookie for username and id. Specify a new cookie if necessary
  app.use(function(req,res,next){
      // check if client has a chat username and user id cookie
      let chat_username = req.cookies.chat_username;
      let chat_user_id = req.cookies.chat_user_id;

      let assign_cookie = false;

      if (chat_username === undefined || chat_user_id === undefined) {
        console.log("Missing cookie field. Assigning new username and id...")
        assign_cookie = true;
      }

      else if(user_list.requires_reset(chat_username, chat_user_id)) {
        console.log("Username and/or ID requires reset...")
        assign_cookie = true;
      }

      if(assign_cookie){
        // Brand new user, assign a username and unique user id
        let new_username = user_list.generate_new_name();
        let new_id = user_list.get_unassigned_id();
        res.cookie('chat_username', new_username, { maxAge: 1000 * 60 * 60 * 24 * 365 * 10, httpOnly: false, sameSite:true });
        res.cookie('chat_user_id', new_id, { maxAge: 1000 * 60 * 60 * 24 * 365 * 10, httpOnly: false, sameSite:true });

        user_list.add_known_user(new_username, new_id);
        console.log(new_username + ' cookies created successfully');
      }

      next();
  });

  // Routes
  app.get('/', (req, res) => {
    res.sendFile(__dirname + '/public/index.html');
  });
  app.use(express.static(__dirname + '/public'));
  app.get('/favicon.ico', (req, res) => {
    // favicon.ico free use licence from: https://www.iconfinder.com/search/?q=MSN
    res.sendFile(__dirname + '/favicon.ico');
  });

  // Websocket Logic
  io.on('connection', (socket) => {
    // Parse cookie for username
    let username = parse_cookie_value(http_cookie=socket.request.headers.cookie, field="chat_username");
    let user_id = parse_cookie_value(http_cookie=socket.request.headers.cookie, field="chat_user_id");
    user_list.connect(username, user_id);

    // Send chat history to newly connected client to get them up to date
    chat_log.send_history(socket);

    // Chat Message Event
    socket.on('chat message', (msg) => {
      let timestamp = new Date();
      chat_log.new_message(timestamp.toLocaleString(), msg, username);
    });

    // Disconnect
    socket.on('disconnect', () => {
      user_list.disconnect(username, user_id);
    });

    // User-specified change requests
    socket.on('username change request', (new_username, callback) => {
      if(new_username === '') callback(false, "Username can't be an empty string");

      if (!user_list.is_valid_name(new_username)) {
        callback(false, 'Username is already taken');
      } else {
        let uname_update = user_list.update_username(user_id, new_username);
        if(uname_update.success) username = new_username;
        callback(uname_update.success, uname_update.message);
      }

      // Update all session userlist and chat windows to reflect the changes
      io.emit("userlist update", user_list.get_connected_users());
      chat_log.send_history();
    });
    socket.on('user color change request', (new_color, callback) => {

      let color_update = user_list.update_color(user_id, new_color);
      if(color_update.success) {
        callback(true, color_update.message);
      } else {
        callback(false, color_update.message);
      }

      // Update all session userlist and chat windows to reflect the changes
      io.emit("userlist update", user_list.get_connected_users());
      chat_log.send_history();
    });
  });

  // Listen on Port 3000
  server.listen(3000, () => {
  console.log('listening on *:3000');
});

  // Ask any browsers that are still connected from the last time the server was
  // up to refresh because the server's memory has reset.
  io.emit('server update');
})();
